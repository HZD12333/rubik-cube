import { createApp } from 'vue'
import '@/sass/index.scss'
import App from './App.vue'
import '@/utils/adapter.ts'

createApp(App).mount('#app')
