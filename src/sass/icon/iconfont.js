/* eslint-disable */
;(window._iconfont_svg_string_ =
  '<svg><symbol id="icon-tupian" viewBox="0 0 1024 1024"><path d="M841.71335 65.290005 182.285626 65.290005c-64.511269 0-116.995621 52.484352-116.995621 116.995621L65.290005 841.71335c0 64.511269 52.484352 116.995621 116.995621 116.995621l659.427724 0c64.511269 0 116.995621-52.484352 116.995621-116.995621L958.708971 182.285626C958.708971 117.774357 906.225643 65.290005 841.71335 65.290005zM182.285626 107.833961l659.427724 0c41.051975 0 74.451666 33.398668 74.451666 74.451666l0 136.557142c-150.09446 5.26184-290.370297 66.084091-396.978337 172.692131-49.960879 49.961902-89.841168 107.331517-118.694309 169.625282-83.496669-70.835302-204.372667-75.376735-292.65841-13.617136L107.833961 182.285626C107.833961 141.232628 141.232628 107.833961 182.285626 107.833961zM107.833961 841.71335 107.833961 702.627618c76.54228-74.311473 198.833511-74.234725 275.272437 0.24457-24.303522 65.298192-37.026288 135.112234-37.026288 206.91149 0 2.223644 0.343831 4.366448 0.977257 6.381337L182.285626 916.165016C141.232628 916.165016 107.833961 882.766348 107.833961 841.71335zM841.71335 916.165016 387.646807 916.165016c0.633427-2.01489 0.977257-4.157693 0.977257-6.381337 0-146.71755 57.053414-284.572244 160.647817-388.166647 98.570993-98.570993 228.166583-154.963351 366.894158-160.204725L916.166039 841.71335C916.165016 882.766348 882.766348 916.165016 841.71335 916.165016z" fill="#272636" ></path><path d="M312.397986 413.458683c60.8376 0 110.332874-49.494251 110.332874-110.332874s-49.494251-110.332874-110.332874-110.332874-110.332874 49.494251-110.332874 110.332874S251.559363 413.458683 312.397986 413.458683zM312.397986 235.337913c37.378306 0 67.788919 30.40959 67.788919 67.788919s-30.40959 67.788919-67.788919 67.788919-67.788919-30.40959-67.788919-67.788919S275.018657 235.337913 312.397986 235.337913z" fill="#272636" ></path></symbol><symbol id="icon-danhangshurukuang" viewBox="0 0 1024 1024"><path d="M896 224H128c-35.2 0-64 28.8-64 64v448c0 35.2 28.8 64 64 64h768c35.2 0 64-28.8 64-64V288c0-35.2-28.8-64-64-64z m0 480c0 19.2-12.8 32-32 32H160c-19.2 0-32-12.8-32-32V320c0-19.2 12.8-32 32-32h704c19.2 0 32 12.8 32 32v384z" fill="#333333" ></path><path d="M224 352c-19.2 0-32 12.8-32 32v256c0 16 12.8 32 32 32s32-12.8 32-32V384c0-16-12.8-32-32-32z" fill="#333333" ></path></symbol><symbol id="icon-rongqi" viewBox="0 0 1024 1024"><path d="M905.6 908.8H118.4a106.88 106.88 0 0 1-106.24-106.88V560.64a106.88 106.88 0 0 1 106.24-106.88h243.84a35.2 35.2 0 0 1 35.2 35.84 114.56 114.56 0 0 0 229.12 0 35.2 35.2 0 0 1 35.2-35.84h243.84a106.88 106.88 0 0 1 106.24 106.88v241.28a106.88 106.88 0 0 1-106.24 106.88z m-787.2-384a35.84 35.84 0 0 0-35.2 35.84v241.28a35.84 35.84 0 0 0 35.2 35.84h787.2a35.84 35.84 0 0 0 35.2-35.84V560.64a35.84 35.84 0 0 0-35.2-35.84h-211.84a184.96 184.96 0 0 1-363.52 0z" fill="#323333" ></path><path d="M53.76 567.68H41.6a35.84 35.84 0 0 1-21.76-45.44L140.8 174.08a35.84 35.84 0 0 1 33.92-23.68h341.76a35.84 35.84 0 1 1 0 71.04H199.68l-112.64 320a35.2 35.2 0 0 1-33.28 26.24z" fill="#323333" ></path><path d="M971.52 567.68a35.84 35.84 0 0 1-33.92-23.68l-112.64-320H508.16a35.84 35.84 0 0 1 0-71.04h342.4a34.56 34.56 0 0 1 33.28 23.68l120.96 343.68a35.2 35.2 0 0 1-21.76 45.44z" fill="#323333" ></path></symbol><symbol id="icon-biaotilan" viewBox="0 0 1025 1024"><path d="M983.04 772.48H44.16a37.76 37.76 0 0 1 0-74.88h938.88a37.76 37.76 0 1 1 0 74.88z" fill="#323333" ></path><path d="M27.52 348.16h211.2V384H154.24v222.08h-42.24V384H27.52zM269.44 348.16h42.24v256h-42.24zM342.4 348.16h211.84V384H469.76v222.08h-42.24V384H342.4zM584.96 348.16h41.6v222.08H768v35.84H584.96zM798.72 348.16h186.24V384h-144v71.68h135.68v36.48h-135.68v78.08h150.4v35.84h-192z" fill="#323333" ></path></symbol></svg>'),
  (function (n) {
    var t = (t = document.getElementsByTagName('script'))[t.length - 1]
    var e = t.getAttribute('data-injectcss')
    var t = t.getAttribute('data-disable-injectsvg')
    if (!t) {
      var a
      var i
      var o
      var c
      var l
      var d = function (t, e) {
        e.parentNode.insertBefore(t, e)
      }
      if (e && !n.__iconfont__svg__cssinject__) {
        n.__iconfont__svg__cssinject__ = !0
        try {
          document.write(
            '<style>.svgfont {display: inline-block;width: 1em;height: 1em;fill: currentColor;vertical-align: -0.1em;font-size:16px;}</style>'
          )
        } catch (t) {
          console && console.log(t)
        }
      }
      ;(a = function () {
        var t
        var e = document.createElement('div')
        // eslint-disable-next-line no-underscore-dangle
        ;(e.innerHTML = n._iconfont_svg_string_),
          (e = e.getElementsByTagName('svg')[0]) &&
            (e.setAttribute('aria-hidden', 'true'),
            (e.style.position = 'absolute'),
            (e.style.width = 0),
            (e.style.height = 0),
            (e.style.overflow = 'hidden'),
            (e = e),
            (t = document.body).firstChild
              ? d(e, t.firstChild)
              : t.appendChild(e))
      }),
        document.addEventListener
          ? ~['complete', 'loaded', 'interactive'].indexOf(document.readyState)
            ? setTimeout(a, 0)
            : ((i = function () {
                document.removeEventListener('DOMContentLoaded', i, !1), a()
              }),
              document.addEventListener('DOMContentLoaded', i, !1))
          : document.attachEvent &&
            ((o = a),
            (c = n.document),
            (l = !1),
            s(),
            (c.onreadystatechange = function () {
              c.readyState == 'complete' && ((c.onreadystatechange = null), h())
            }))
    }
    function h() {
      // eslint-disable-next-line no-unused-expressions
      l || ((l = !0), o())
    }
    function s() {
      try {
        c.documentElement.doScroll('left')
      } catch (t) {
        return void setTimeout(s, 50)
      }
      h()
    }
  })(window)
